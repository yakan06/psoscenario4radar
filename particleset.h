#ifndef PARTICLESET_H
#define PARTICLESET_H

#include <QObject>
#include <QVector>
#include <QPoint>
class Particle;

class ParticleSet : public QObject
{
    Q_OBJECT
public:
    explicit ParticleSet(QObject *parent = nullptr);

    int count =1;

    QVector<Particle*> m_Set;

    virtual Particle* getSet(int i)=0;

    virtual void genereateSet()=0;

    virtual double getAmacValue()=0;


     int minErrorP=1000000;
     static int minErrorG;

    // QVector<Particle*> *m_SetPrivateBest;
     //static QVector<Particle*> *m_SetGlobalBest;
      QPoint m_SetPrivateBest;
      static QPoint m_SetGlobalBest;


signals:

public slots:
};

#endif // PARTICLESET_H
