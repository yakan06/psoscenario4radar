#include "particle.h"
#include "utilities.h"

QPoint Particle::positionGBest=QPoint(-1,-1);

Particle::Particle(QObject *parent) : QObject(parent)
{

}

double Particle::Random_0_1()
{

    double r = rand()%100;

    r=r/100;

    return r;

}

double Particle::calculate_v_X()
{
    double newV=0;

     //Vik_1 = Vik + c1*rand1k(pbestik-xik)+ c2*rand2k(gbestk-xik);
    // Xik_1 = xik + Vik+1;

    newV = c1*Random_0_1()*(positionPBest.x()-position.x())+c2*Random_0_1()*(positionGBest.x()-position.x());

    return newV;
}

double Particle::calculate_v_Y()
{
    double newV=0;

     //Vik_1 = Vik + c1*rand1k(pbestik-xik)+ c2*rand2k(gbestk-xik);
    // Xik_1 = xik + Vik+1;

    newV = c1*Random_0_1()*(positionPBest.y()-position.y())+c2*Random_0_1()*(positionGBest.y()-position.y());

    return newV;
}


