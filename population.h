#ifndef POPULATION_H
#define POPULATION_H

#include <QObject>
#include <QVector>

class RadarSet;

class Population : public QObject
{
    Q_OBJECT
public:
    explicit Population(QObject *parent = nullptr);

     QVector<RadarSet*> m_radarSet;

     int size;



     void generate();

signals:

public slots:
};

#endif // POPULATION_H
