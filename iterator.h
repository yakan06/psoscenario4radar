#ifndef ITERATOR_H
#define ITERATOR_H

#include <QObject>
#include <QTimer>
#include <QVector>
#include "radar.h"
#include "enviroment.h"

class ParticleSet;
class Population;

class iterator : public QObject
{
    Q_OBJECT
public:
    explicit iterator(QObject *parent = nullptr);
    int iteratorTime = 100;


    QTimer m_timer;



     int populationCount = 10;




signals:

public slots:
     void newGeneration(Population* population);
     void createPopulation(Population*  population );


     bool testGeneration(Population* population);
};

#endif // ITERATOR_H
