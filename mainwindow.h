#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "radar.h"
#include <QVector>
#include "iterator.h"
#include "enviroment.h"

class Population;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:
    Ui::MainWindow *ui;

    Population *m_Population;


   iterator i;
   int ii= 0;

protected:
    void paintEvent(QPaintEvent *event);
 public slots:
    void nextGeneration();
};

#endif // MAINWINDOW_H
