#include "mainwindow.h"
#include "ui_mainwindow.h"
 #include <QPainter>
#include "enviroment.h"
#include "radarset.h"
#include "population.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_Population = new Population();

    m_Population->size = 10;

    i.createPopulation(m_Population);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{

   //     std::cout<<":::::::::::::::::"<<std::endl;
//Enviroment enviroment;

    int virtualWith = 500;

    int pX= 190;
    int pY= 40;

    QPainter painter(this);

    painter.drawRect(pX, pY,virtualWith, virtualWith);

    for(int i= 0;i< m_Population->size;++i)
    {

    RadarSet &currentSet = *m_Population->m_radarSet[i];

   // Radar* radar = (Radar*)&currentSet.m_Set[0];

    int x= currentSet.m_Set[0]->position.x();
    int y= currentSet.m_Set[0]->position.y();


    ui->x->setText(  QString::number(x));
     ui->y->setText(  QString::number(y) );
    /* ui->x2->setText(  QString::number(m_radar[1]->position.x()) );
    ui->x3->setText(  QString::number(m_radar[2]->position.x()) );
    ui->x4->setText(  QString::number(m_radar[3]->position.x()) );
    ui->x5->setText(  QString::number(m_radar[4]->position.x()) );
    ui->x6->setText(  QString::number(m_radar[5]->position.x()) );
    ui->x7->setText(  QString::number(m_radar[6]->position.x()) );
    ui->x8->setText(  QString::number(m_radar[7]->position.x()) );
    ui->x9->setText(  QString::number(m_radar[8]->position.x()) );
    ui->x10->setText(  QString::number(m_radar[9]->position.x()) );


    ui->y2->setText(  QString::number(m_radar[1]->position.y()) );
    ui->y3->setText(  QString::number(m_radar[2]->position.y()) );
    ui->y4->setText(  QString::number(m_radar[3]->position.y()) );
    ui->y5->setText(  QString::number(m_radar[4]->position.y()) );
    ui->y6->setText(  QString::number(m_radar[5]->position.y()) );
    ui->y7->setText(  QString::number(m_radar[6]->position.y()) );
    ui->y8->setText(  QString::number(m_radar[7]->position.y()) );
    ui->y9->setText(  QString::number(m_radar[8]->position.y()) );
    ui->y10->setText(  QString::number(m_radar[9]->position.y()) );
*/



         int x1 = (1.0*x)*(virtualWith/100);
         int y1 = (1.0*y)*(virtualWith/100);


          painter.drawEllipse(x1+pX,y1+pY,10,10);
  ui->lineEdit->setText(QString::number(currentSet.minErrorG));
     }




     /*
   foreach (RadarSet *var, m_radarSet) {


       // painter.drawText(x+300+10,y+100+10,QString::number(var->position.x())+QString(":")+QString::number(var->position.y()));
    }
*/
}

void MainWindow::nextGeneration()
{
        i.testGeneration(m_Population);

        i.newGeneration(m_Population);

    update();



    ii++;

    ui->lineEdit_i->setText(QString::number(ii));


}
