#include "iterator.h"
#include "positionupdater.h"
#include "enviroment.h"
#include "particleset.h"
#include "population.h"
#include "radarset.h"
#include "iostream"

iterator::iterator(QObject *parent) : QObject(parent)
{

}

void iterator::newGeneration(Population* population)
{
    PositionUpdater u;

    for(int i= 0;i< population->size;++i)
    {
        RadarSet *currentSet = population->m_radarSet[i];

        int vx =currentSet->m_Set[0]->calculate_v_X();
        int vy =currentSet->m_Set[0]->calculate_v_Y();

        u.update(*currentSet->m_Set[0],vx,vy);
        std::cout<<currentSet->m_Set[0]->position.x()<<":"<<currentSet->m_Set[0]->position.y()<<std::endl;
    }

}

void iterator::createPopulation(Population* population)
{
    population->generate();
}

bool iterator::testGeneration(Population* population)
{

    for(int i= 0;i< population->size;++i)
    {
        RadarSet &currentSet = *population->m_radarSet[i];

      int error = currentSet.getAmacValue();

      if(error < currentSet.minErrorP ){


          currentSet.minErrorP = error;

          currentSet.m_SetPrivateBest = currentSet.m_Set;

      //   currentSet.m_Set[0]->positionPBest =  currentSet.m_Set[0]->position;


      }

      if(error < currentSet.minErrorG ){

             currentSet.minErrorG = error;
           // currentSet.m_SetGlobalBest = &currentSet.m_Set;
    //    currentSet.m_SetGlobalBest = currentSet.m_Set[0]->position;
currentSet.m_Set[0]->positionGBest =  currentSet.m_Set[0]->position;
        //  radar->positionGBest.setX(x);
         //  radar->positionGBest.setY(y);
      }

    }

    return false;

}

