#ifndef PARTICLE_H
#define PARTICLE_H

#include <QObject>
#include "utilities.h"
#include <QPoint>

class Particle : public QObject
{
    Q_OBJECT
public:
    explicit Particle(QObject *parent = nullptr);

    double c1=2;
    double c2=2;

    //static double gBest;
    //double pBest=MinValue;

    double vX;
    double vY;

    double calculate_v_X();
     double calculate_v_Y();

     QPoint position;

     virtual void move(int mx,int my)=0;

     QPoint positionPBest=QPoint(-1,-1);
     static QPoint positionGBest;

      int minErrorP=1000000;

      double Random_0_1();

};

#endif // PARTICLE_H
